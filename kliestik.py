#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Kliestik counter
 
@author: (c) Richard Czikhardt, czikhardt.richard@gmail.com
10.1.2020
"""

import sys
import numpy as np
import cv2
#from scipy.stats import median_absolute_deviation
from skimage.util import invert
from skimage.feature import blob_log
from skimage.color import rgb2gray
import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning) 
# ============================================================================

def kliestik_counter(img, width_platna=580, height_platna=450):
    # hardcoded params:
    # ---
    # - kliestik size:
    #min_d = 1.8 # perimeter
    min_d = 1.4
    max_d = 1.8
    #max_d = 2.2
    # - patch radius factor for intensity averaging:
    fact = 2.5
    # - intensity threshold for averaged patches:
    thr = 0.2
    # ---
    # convert to grayscale:
    image_gray = rgb2gray(img)
    inverted = invert(image_gray)
    #inverted = exposure.equalize_hist(inverted)
    # predict min/max sigma (Gauss kernel)
    # sigma = r/sqrt(2)
    # width_platna = 450 mm
    # width_image = 3300 pix
    width_img = np.max(img.shape[:2])
    pix_size = max(width_platna, height_platna)/width_img
    min_sigma = int(min_d/pix_size/np.sqrt(2))
    max_sigma = int(max_d/pix_size/np.sqrt(2))
    # Laplace of Gaussian detector:
    blobs = blob_log(inverted, min_sigma=min_sigma,max_sigma=max_sigma, 
                 threshold=.1, overlap = 0.9)
    # convert to radius:
    blobs[:, 2] = blobs[:, 2] * np.sqrt(2)
    # filter them by min. intensity:
    # - average over patch
    radius= (blobs[:,2]/fact).astype(int)
    x = blobs[:,0].astype(int)
    y = blobs[:,1].astype(int)
    q = np.empty(len(x))
    for i in range(len(radius)):
        min_x = x[i]-radius[i] if x[i]>radius[i] else 0
        max_x = x[i]+radius[i] if x[i]+radius[i] < img.shape[0] else img.shape[0]
        min_y = y[i]-radius[i] if y[i]>radius[i] else 0
        max_y = y[i]+radius[i] if y[i]+radius[i] < img.shape[1] else img.shape[1]
        q[i] = np.nanmean(image_gray[min_x:max_x,min_y:max_y])
    q[np.isnan(q)] = 1
    #thr =  np.median(q) + 3*1.486*ss.median_absolute_deviation(q)
    #thr =  np.median(q) + 1.486*ss.median_absolute_deviation(q)
    #thr =  np.median(q) + median_absolute_deviation(q)
    blobs = blobs[q < thr]
    return blobs

def output_creator(img, blobs, out_file):
    image_out = img.copy()
    # write blobs:
    for blb in blobs:
        cv2.circle(image_out, (int(blb[1]),int(blb[0])), int(blb[2]), 
                   (0,0,255), 2)
    # log number of blobs
    cv2.putText(image_out,str(len(blobs)),(0,image_out.shape[0]-10), 
                cv2.FONT_HERSHEY_SIMPLEX, 4, (0, 0, 255), 5, cv2.LINE_AA)
    return image_out

def main():
    in_file = sys.argv[1]
    out_file = sys.argv[2]
    try:
        # read cropped:
        cropped = cv2.imread(in_file)  
        # do the job:
        if len(sys.argv) > 3:
            width = int(sys.argv[3])
            height = int(sys.argv[4])
            blobs = kliestik_counter(cropped, width, height)
        else:
            blobs = kliestik_counter(cropped)
        image_out = output_creator(cropped, blobs, out_file)
        # write image:
        cv2.imwrite(out_file,image_out)
        sys.exit(0)
    except:
        sys.exit(1)

# ============================================================================

if __name__ == "__main__":
    main()    

