#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Kliestik counter
 
    Program na pocitanie kliestikov na bielej platni.
 
@author: (c) Richard Czikhardt, czikhardt.richard@gmail.com
10.1.2020
"""

import numpy as np
import cv2
import tkinter as tk
from tkinter import messagebox as mb
from tkinter import filedialog as fd 
#from scipy.stats import median_absolute_deviation
from skimage.util import invert
from skimage.feature import blob_log
from skimage.color import rgb2gray
# constants
FINAL_LINE_COLOR = (0,0,255)
WORKING_LINE_COLOR = (0,0,255)

# ============================================================================
#root.mainloop()

class PolygonDrawer(object):
    def __init__(self, window_name):
        self.window_name = window_name # Name for our window
        self.done = False # Flag signalling we're done
        self.current = (0, 0) # Current position, so we can draw the line-in-progress
        self.points = [] # List of points defining our polygon

    def on_mouse(self, event, x, y, buttons, user_param):
        # Mouse callback that gets called for every mouse event (i.e. moving, clicking, etc.)

        if self.done: # Nothing more to do
            return

        if event == cv2.EVENT_MOUSEMOVE:
            # We want to be able to draw the line-in-progress, so update current mouse position
            self.current = (x, y)
        elif event == cv2.EVENT_LBUTTONDOWN:
            # Left click means adding a point at current position to the list of points
            print("Adding point #%d with position(%d,%d)" % (len(self.points), x, y))
            self.points.append((x, y))
        elif event == cv2.EVENT_RBUTTONDOWN:
            # Right click means we're done
            print("Completing polygon with %d points." % len(self.points))
            self.done = True

    def run(self):
        # Let's create our working window and set a mouse callback to handle events
        #cv2.namedWindow(self.window_name, flags=cv2.WINDOW_AUTOSIZE)
        cv2.namedWindow(self.window_name,cv2.WINDOW_NORMAL)
        #cv2.imshow(self.window_name, np.zeros(CANVAS_SIZE, np.uint8))
        cv2.imshow(self.window_name, image)
        cv2.waitKey(1)
        cv2.setMouseCallback(self.window_name, self.on_mouse)

        while(len(self.points) < 4):
            # This is our drawing loop, we just continuously draw new images
            # and show them in the named window
            #canvas = np.zeros(CANVAS_SIZE, np.uint8)
            canvas = image.copy()
            if (len(self.points) > 0):
                # Draw all the current polygon segments
                cv2.polylines(canvas, np.array([self.points]), False, 
                              FINAL_LINE_COLOR, 2, lineType=cv2.LINE_AA)
                # And  also show what the current segment would look like
                cv2.line(canvas, self.points[-1], self.current, WORKING_LINE_COLOR)
            # Update the window
            cv2.imshow(self.window_name, canvas)
            # And wait 50ms before next iteration (this will pump window messages meanwhile)
            if cv2.waitKey(50) == 27: # ESC hit
                self.done = True

        # User finised entering the polygon points, so let's make the final drawing
        #canvas = np.zeros(CANVAS_SIZE, np.uint8)
        canvas = image.copy()
        # of a filled polygon
        if (len(self.points) > 0):
            cv2.fillPoly(canvas, np.array([self.points]), FINAL_LINE_COLOR)
        # And show it
        cv2.imshow(self.window_name, canvas)
        # Waiting for the user to press any key
        cv2.waitKey(500)
        cv2.destroyWindow(self.window_name)
        return canvas

def callback():
    cv2.destroyAllWindows()

def fd_callback():
    name= fd.askopenfilename() 
    return name

def kliestik_counter(img, width_platna=550):
    # hardcoded params:
    # ---
    # - kliestik size:
    min_d = 1.8 # perimeter
    max_d = 2.2
    # - patch radius factor for intensity averaging:
    fact = 2.5
    # - intensity threshold for averaged patches:
    thr = 0.2
    # ---
    # convert to grayscale:
    image_gray = rgb2gray(img)
    inverted = invert(image_gray)
    #inverted = exposure.equalize_hist(inverted)
    # predict min/max sigma (Gauss kernel)
    # sigma = r/sqrt(2)
    # width_platna = 600 mm
    # width_image = 3600 pix
    width_img = np.max(img.shape[:2])
    pix_size = width_platna/width_img
    min_sigma = int(min_d/pix_size/np.sqrt(2))
    max_sigma = int(max_d/pix_size/np.sqrt(2))
    # Laplace of Gaussian detector:
    blobs = blob_log(inverted, min_sigma=min_sigma,max_sigma=max_sigma, 
                 threshold=.1, overlap = 0.9)
    # convert to radius:
    blobs[:, 2] = blobs[:, 2] * np.sqrt(2)
    # filter them by min. intensity:
    # - average over patch
    radius= (blobs[:,2]/fact).astype(int)
    x = blobs[:,0].astype(int)
    y = blobs[:,1].astype(int)
    q = np.empty(len(x))
    for i in range(len(radius)):
        q[i] = np.nanmean(image_gray[x[i]-radius[i]:x[i]+radius[i],
                                     y[i]-radius[i]:y[i]+radius[i]])
    q[np.isnan(q)] = 1
    #thr =  np.median(q) + 3*1.486*ss.median_absolute_deviation(q)
    #thr =  np.median(q) + 1.486*ss.median_absolute_deviation(q)
    #thr =  np.median(q) + median_absolute_deviation(q)
    blobs = blobs[q < thr]
    return blobs

def output_creator(img, blobs, out_file):
    image_out = img.copy()
    # write blobs:
    for blb in blobs:
        cv2.circle(image_out, (int(blb[1]),int(blb[0])), int(blb[2]), 
                   (0,0,255), 2)
    # log number of blobs
    cv2.putText(image_out,str(len(blobs)),(0,image_out.shape[0]-10), 
                cv2.FONT_HERSHEY_SIMPLEX, 4, (0, 0, 255), 5, cv2.LINE_AA)
    # write image:
    cv2.imwrite(out_file,image_out)
    return image_out

# ============================================================================

if __name__ == "__main__":
    
    root = tk.Tk()
    root.withdraw()
    name = fd.askopenfilename(title='Vyber snimku platne...') 
    infile = name
    
    #% load the image and interactive crop
    image = cv2.imread(infile)
    height = image.shape[0]
    width = image.shape[1]
    mask = np.zeros((height, width), dtype=np.uint8)
    
    mb.showinfo('Info', 'Definuj 4 rohy platne.')
    root.update()
    
    pd = PolygonDrawer("Polygon")
    #image = pd.run()
    pd.run()
    #cv2.imwrite("polygon.png", image)
    print("Polygon = %s" % pd.points)
    
    points = np.array([pd.points])
    
    cv2.fillPoly(mask, points, (255))

    res = cv2.bitwise_and(image,image,mask = mask)
    
    rect = cv2.boundingRect(points) # returns (x,y,w,h) of the rect
    im2 = np.full((res.shape[0], res.shape[1], 3), (255, 255, 255), dtype=np.uint8 ) # you can also use other colors or simply load another image of the same size
    maskInv = cv2.bitwise_not(mask)
    colorCrop = cv2.bitwise_or(im2,im2,mask = maskInv)
    finalIm = res + colorCrop
    cropped = finalIm[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]
    #cropped = res[rect[1]: rect[1] + rect[3], rect[0]: rect[0] + rect[2]]
    
    #cv2.namedWindow("cropped",cv2.WINDOW_NORMAL)
    #cv2.imshow("cropped" , cropped )
    #cv2.imshow("same size" , res)
    #cv2.waitKey(27)
    
    # ask for out file
    #outName= fd.askopenfilename(title='Vyber snimku platne...')
    #outName = fd.askdirectory(title='Vyber kam ulozit vysledok...')
    outName = fd.asksaveasfilename(title='Vyber kam ulozit vysledok...',
                                   defaultextension=".JPG", 
                                   filetypes=(("JPG file", "*.JPG"),
                                              ("All Files", "*.*")))
    #outfile = outName
    out_file = outName 
    
    ##############
    # actual computation
    top = tk.Toplevel()
    top.title('Pocitam...')
    WELCOME_MSG = 'Pocitam kliestikov...'
    tk.Message(top, text=WELCOME_MSG, padx=20, pady=20).pack()
    root.update()
    
    # do the job:
    blobs = kliestik_counter(cropped)
    image_out = output_creator(cropped, blobs, out_file)
    
    top.destroy()
    cv2.namedWindow("Vysledok",cv2.WINDOW_NORMAL)
    cv2.imshow("Vysledok" , image_out )
    #if cv2.waitKey(50) == 27:
    #    cv2.destroyWindow("Vysledok")
    cv2.waitKey(5000)
    cv2.destroyWindow("Vysledok")
    
    mb.showinfo('Info', 'Pocet kliestikov = ' + str(len(blobs)))
    root.update()